package estudo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe responsável por .
 *
 * @author Christian Henrique Rezende
 * @version 1.0
 * @since Release 01
 */
public abstract class BaseCompareService<E extends BaseEntity, M extends BaseController<E>> {
	private List<E> entityListNow;
	private List<E> entityListNew;
	private boolean deletable;
	private M controller;

	/**
	 *
	 * @param deletable
	 *            indica se a entidade pode ser deletada
	 */
	public BaseCompareService(boolean deletable, M controller) {
		this.controller = controller;
		this.deletable = deletable;
	}

	public void compareObjects(List<E> entityListNow, List<E> entityListNew) {
		Map<Object, Object> mapaPosicao = new HashMap<Object, Object>();
		this.entityListNow = entityListNow;
		this.entityListNew = entityListNew;
		boolean existe = false;
		int posicaoListNow = 0;

		// ------------------------------Compara------------------------------------
		System.out.println("-----------------Comparando (Create e Update) -----------------");
		existe = false;
		List<E> unMatchList = new ArrayList<>();
		List<E> matches = new ArrayList<>();

		for (E entity : entityListNow) {
			unMatchList.add(entity);
		}

		// Verifica se as listas não estão vazias
		if (this.entityListNow != null && !this.entityListNow.isEmpty() && this.entityListNew != null
				&& !this.entityListNew.isEmpty()) {

			// Compara as listas e identifica o que fazer com eles

			for (E entityNovo : this.entityListNew) {
				posicaoListNow = 0;
				for (E entityAntigo : this.entityListNow) {

					if (entityAntigo.get_id() == entityNovo.get_id()) {
						existe = true;

						if (entityAntigo.equals(entityNovo)) {
							System.out.println("Entity ja existe");
							unMatchList.remove(entityAntigo);
						} else {
							System.out.println("Entity deve ser atualizado");
							mapaPosicao.put(entityAntigo, entityNovo);
							matches.add(entityAntigo);
						}
						posicaoListNow++;
						break;
					}
				}
				if (existe) {

					existe = false;

				} else {
					System.out.println("Entity não existe");
					create(entityNovo);
					existe = false;
				}

				existe = false;
			}
		}

		System.out.println(
				"-----------------------------Verificando entidades não deletadas----------------------------");
		// Verifica quais serão deletados

		for (E match : matches) {
			System.out.println(match.get_id());
			update(match, (E) mapaPosicao.get(match));
			unMatchList.remove(match);
		}

		if (deletable) {

			System.out.println(
					"-----------------------------Verificando entidades deletadas----------------------------");

			// Remove a entidade
			for (E unMatch : unMatchList) {
				System.out.println(unMatch.get_id());
				delete(unMatch);
			}
		} else {
			System.out.println(
					"----------------------------Esta entidade nao pode ser deletada----------------------------");
		}

		// ------------------------------Compara------------------------------------
		// Print a lista final
		System.out.println("-----------------Comparando (Show new list) -----------------");

		if (this.entityListNow != null && !this.entityListNow.isEmpty()) {
			System.out.println("---------------entityList Now-------------");

			for (E entity : this.entityListNow) {
				System.out.println(entity.get_id() + " = " + entity.get_id());
			}
		}
	}

	private void create(E entity) {

		System.out.println("Criou novo usuário");
		this.entityListNow.add(entity);
		controller.create(entity);
	}

	private void update(E entityOld, E entityNew) {
		this.entityListNow.remove(entityOld);
		this.entityListNow.add(entityNew);
		controller.update(entityNew);
		System.out.println("Atualizou o entity");
	}

	private void delete(E entity) {
		this.entityListNow.remove(entity);
		controller.delete(entity);
		System.out.println("Removendo entity antigo");

	}
}
