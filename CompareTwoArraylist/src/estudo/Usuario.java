package estudo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Usuario extends BaseEntity{
	private String nome;
	private Timestamp time;	
	private Imagem imagem;
	private List<Produto> produtoList = new ArrayList<>();
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
		
	public Timestamp getTime() {
		return time;
	}
	public void setTime(Timestamp time) {
		this.time = time;
	}
	
	public Imagem getImagem() {
		return imagem;
	}
	public void setImagem(Imagem imagem) {
		this.imagem = imagem;
	}	
	
	
	public List<Produto> getProdutoList() {
		return produtoList;
	}
	public void setProdutoList(List<Produto> produtoList) {
		this.produtoList = produtoList;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		 //se nao forem objetos da mesma classe sao objetos diferentes
        if(!(obj instanceof Usuario)) return false; 

        //se forem o mesmo objeto, retorna true
        if(obj == this) return true;

        // aqui o cast � seguro por causa do teste feito acima
        Usuario entity = (Usuario) obj; 

        //aqui voc� compara a seu gosto, o ideal � comparar atributo por atributo
        return this.get_id()  == entity.get_id() &&
                this.nome.equals(entity.getNome()) 
                && this.time.equals(entity.getTime())&&
                this.imagem.equals(entity.getImagem())
                && this.produtoList.equals(entity.getProdutoList());
        
	}
	
}
