package estudo;

public class Produto extends BaseEntity{
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		 //se nao forem objetos da mesma classe sao objetos diferentes
        if(!(obj instanceof Produto)) return false; 

        //se forem o mesmo objeto, retorna true
        if(obj == this) return true;

        // aqui o cast � seguro por causa do teste feito acima
        Produto entity = (Produto) obj; 

        //aqui voc� compara a seu gosto, o ideal � comparar atributo por atributo
        return this.get_id()  == entity.get_id() &&
                this.nome.equals(entity.getNome());
        
	}
	
}
