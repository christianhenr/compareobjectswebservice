package estudo;




/**
 * Classe responsável por .
 *
 * @author Christian Henrique Rezende
 * @version 1.0
 * @since Release 01
 */
public class UsuarioController extends BaseController<Usuario> {
    public UsuarioController() {
        
    }

    @Override
    public Long create(Usuario entity) {
        System.out.println("Criando usuario" + entity.getNome());
        return super.create(entity);
    }

    @Override
    public void update(Usuario entity) {
        System.out.println("atualizando usuario" + entity.getNome());
        super.update(entity);
    }

    @Override
    public void delete(Usuario entity) {
        System.out.println("Deletando usuario" + entity.getNome());
        super.delete(entity);
    }
}
