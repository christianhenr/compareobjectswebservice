package estudo;


/**
 * Classe responsável por .
 *
 * @author Christian Henrique Rezende
 * @version 1.0
 * @since Release 01
 */
public class UsuarioServiceCompare extends BaseCompareService<Usuario,UsuarioController> {

    public UsuarioServiceCompare(boolean deletable, UsuarioController controller) {
        super(deletable, controller);
    }
}
