package estudo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class Main {

	
	private static Timestamp timestampNow;
	private static Timestamp timestampNew;
	private static Imagem imagem1;
	private static Imagem imagem2;
	private static ArrayList produtoList2;
	private static ArrayList produtoList1;
	private static ArrayList produtoList3;

	public static void main(String[] args) {
		List<Usuario> entityListNow = new ArrayList<>();
		List<Usuario> entityListNew = new ArrayList<>();
		
		//-------------------Atual---------------------------------
		entityListNow = populaNow();
		if(entityListNow!=null && !entityListNow.isEmpty()){
			System.out.println("---------------entityList Now-------------");

			for (Usuario usuario : entityListNow) {
				System.out.println(usuario.get_id() +" = "+ usuario.getNome() + " : " + usuario.getTime().toString());
			}
		}
		
		//----------------Novo-------------------------------------
		entityListNew = populaNew();
		if(entityListNew!=null && !entityListNew.isEmpty()){
			System.out.println("---------------entityList New-------------");

			for (Usuario usuario : entityListNew) {
				System.out.println(usuario.get_id() +" = "+ usuario.getNome() + " : " + usuario.getTime().toString());
			}
		}
		UsuarioController controller = new UsuarioController();
		UsuarioServiceCompare compare = new UsuarioServiceCompare(true,controller);
		compare.compareObjects(entityListNow,entityListNew);
	}


	
	private static List<Usuario> populaNow() {
		timestampNow = new Timestamp(System.currentTimeMillis());
		List<Usuario> entityListNow = new ArrayList<>();
		Usuario usuario = new Usuario();
		imagem1 = new Imagem();
		imagem1.set_id(1L);
		imagem1.setNome("aa.png");
		imagem2 = new Imagem();
		imagem2.set_id(2L);
		imagem2.setNome("ab.png");
		
		produtoList1 = new ArrayList<>();
		Produto produto1 = new Produto();
		produto1.set_id(1L);
		produto1.setNome("AA");
		produtoList1.add(produto1);
		Produto produto2 = new Produto();
		produto2.set_id(2L);
		produto2.setNome("BB");
		produtoList1.add(produto2);

		
		produtoList2 = new ArrayList<>();

		Produto produto3 = new Produto();
		produto3.set_id(3L);
		produto3.setNome("CC");
		produtoList2.add(produto3);

		Produto produto4 = new Produto();
		produto4.set_id(4L);
		produto4.setNome("DD");
		produtoList2.add(produto4);
		
		produtoList3 = new ArrayList<>();

		Produto produto5 = new Produto();
		produto5.set_id(1L);
		produto5.setNome("AA");
		produtoList3.add(produto5);

		Produto produto6 = new Produto();
		produto6.set_id(8L);
		produto6.setNome("BB");
		produtoList3.add(produto6);

		
		
		usuario.set_id(1L);
		usuario.setNome("Joao Paulo");
		usuario.setTime(timestampNow);
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList1);
		
		entityListNow.add(usuario);
		
		usuario = new Usuario();
		usuario.set_id(2L);
		usuario.setNome("Joaquim dos Santos");
		usuario.setTime(timestampNow);
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList1);

		
		entityListNow.add(usuario);
		
		usuario = new Usuario();
		usuario.set_id(3L);
		usuario.setNome("Antonio");
		usuario.setTime(timestampNow);
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList1);

		
		entityListNow.add(usuario);
		
		usuario = new Usuario();
		usuario.set_id(4L);
		usuario.setNome("Jaracura");
		usuario.setTime(timestampNow);		
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList1);

		entityListNow.add(usuario);
		
		usuario = new Usuario();
		usuario.set_id(6L);
		usuario.setNome("Raul");
		usuario.setTime(timestampNow);		
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList1);

		entityListNow.add(usuario);
		
		usuario = new Usuario();
		usuario.set_id(7L);
		usuario.setNome("Plock das Listas diferentes");
		usuario.setTime(timestampNow);		
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList1);

		entityListNow.add(usuario);
		
		usuario = new Usuario();
		usuario.set_id(8L);
		usuario.setNome("Elvis Igual");
		usuario.setTime(timestampNow);		
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList1);

		entityListNow.add(usuario);
		
		
		usuario = new Usuario();
		usuario.set_id(9L);
		usuario.setNome("Plock das Listas diferentes com produtos iguais");
		usuario.setTime(timestampNow);		
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList1);

		entityListNow.add(usuario);
		
		return entityListNow;
	}
	
	private static List<Usuario> populaNew() {
		timestampNew = new Timestamp(System.currentTimeMillis());
		List<Usuario> entityListNew = new ArrayList<>();
		Usuario usuario = new Usuario();
		
		usuario.set_id(1L);
		usuario.setNome("Joao Paulo");
		usuario.setTime(timestampNew);
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList1);


		
		entityListNew.add(usuario);
		
		usuario = new Usuario();
		usuario.set_id(3L);
		usuario.setNome("Joana Maria");
		usuario.setTime(timestampNow);
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList1);


		entityListNew.add(usuario);
		
		usuario = new Usuario();
		usuario.set_id(5L);
		usuario.setNome("Ultimina dos Santos");
		usuario.setTime(timestampNow);
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList1);


		entityListNew.add(usuario);
		
		usuario = new Usuario();
		usuario.set_id(6L);
		usuario.setNome("Raul das imagem diferente");
		usuario.setTime(timestampNow);		
		usuario.setImagem(imagem2);
		usuario.setProdutoList(produtoList1);


		entityListNew.add(usuario);
		
		usuario = new Usuario();
		usuario.set_id(7L);
		usuario.setNome("Plock das Listas diferentes");
		usuario.setTime(timestampNow);		
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList2);

		entityListNew.add(usuario);
		
		usuario = new Usuario();
		usuario.set_id(8L);
		usuario.setNome("Elvis Igual");
		usuario.setTime(timestampNow);		
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList1);
		entityListNew.add(usuario);
		
		usuario = new Usuario();
		usuario.set_id(9L);
		usuario.setNome("Plock das Listas diferentes com produtos iguais");
		usuario.setTime(timestampNow);		
		usuario.setImagem(imagem1);
		usuario.setProdutoList(produtoList3);

		entityListNew.add(usuario);
		
		return entityListNew;
	}
}
